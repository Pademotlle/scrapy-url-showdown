import constants
import json
import aio_pika
import asyncio


class MsgBrokerManager:
    """
    Consumer that gets the event to retrieve URL battles
    """

    def __init__(self):
        """
        Class constructor
        """

        self.received_events = asyncio.Queue(maxsize=1000)
        self.connection = None
        self.pika_channel = None
        self.queue = None
        self.exchange = None

    @property
    async def get_received_event(self):
        return await self.received_events.get()

    async def connect_to_msg_broker(self, event_loop):
        """
        This method establishes connection and creates a channel to the RabbitMQ message broker
        """

        print("Connecting to RabbitMQ...")
        self.connection = await aio_pika.connect_robust(
            constants.RABBITMQ_URL,
            loop=event_loop
        )
        self.pika_channel = await self.connection.channel()
        self.queue = await self.pika_channel.get_queue(constants.EVENT_QUEUE)
        self.exchange = await self.pika_channel.get_exchange(constants.URL_EXCHANGE)

    def close_broker_connection(self):
        """
        This method closes the connection with RabbitMQ
        """

        print("Closing RabbitMQ connection...")
        self.connection.close()
        print("RabbitMQ connection closed!")

    async def on_message_reception(self, message: aio_pika.IncomingMessage):
        """
        on_message doesn't necessarily have to be defined as async.
        Here it is to show that it's possible.
        """

        print(f" [x] Event received")
        await self.received_events.put(message.body.decode("utf-8"))

    async def consume(self):
        """
        This method listens continually if there are new events in queue

        Returns:
            dictionary that contains the battle format
        """

        print("Start consuming")
        await asyncio.sleep(0.5)
        await self.queue.consume(self.on_message_reception, no_ack=True)

    async def publish(self, battle_urls: dict):
        """
        This method publishes into a RabbitMQ exchange all URLs retrieved from a given Pokemon
        battle format.

        Args:
            battle_urls: dictionary that contains all URLs listed for a
                        given battle_format (dict key).
                        i.e.:
                            {
                                "Monotype": ["url1", "url2"]
                            }

        """

        battle_format = list(battle_urls.keys()).pop()

        for battle_url in battle_urls[battle_format]:

            print(f"Publishing battle URl to {constants.URL_EXCHANGE} exchange: {battle_url}")
            await self.exchange.publish(
                aio_pika.Message(bytes(json.dumps({battle_format: battle_url}), encoding="utf-8")),
                routing_key=""
            )
