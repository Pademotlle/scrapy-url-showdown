import asyncio

from scrappy_worker import miltank
from rabbitmq_manager import scorbunny


class ScrapyException(Exception):
    """
    Base Exception
    """


class UrlScraper:
    """
    This class scraps the battle URLs from PokemonShowdown web page when event is received
    """

    def __init__(self):
        """
        Class Constructor
        """

        self.broker = scorbunny.MsgBrokerManager()
        self.worker = miltank.WebScraper()

    async def connect_to_broker(self, event_loop):
        """
        This method established connection with the RabbitMQmessage broker
        """

        await self.broker.connect_to_msg_broker(event_loop=event_loop)

    async def start_consuming(self):
        """
        This method makes the RabbitMQ client to start consuming
        """

        await self.broker.consume()

    async def publish_battle_urls(self):
        """
        This method publishes the scraped battle URLs to a RabbitMQ exchange
        """

        while True:
            try:
                battle_urls = await self.worker.get_battle_urls
                await self.broker.publish(battle_urls=battle_urls.pop())

            except IndexError:
                pass

    async def scrape_battle_urls(self):
        """
        This method scrapes all current battles for the given battle format from the consumed event
        """

        while True:
            try:
                event = await self.broker.get_received_event
                await self.worker.scrape(battle_format=event)

            except IndexError:
                pass


if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    scrapy_worker = UrlScraper()

    try:
        future_tasks = asyncio.gather(
            scrapy_worker.connect_to_broker(event_loop=loop),
            scrapy_worker.start_consuming(),
            scrapy_worker.scrape_battle_urls(),
            scrapy_worker.publish_battle_urls()
        )
        loop.run_until_complete(future_tasks)

    except ScrapyException:
        scrapy_worker.broker.close_broker_connection()
