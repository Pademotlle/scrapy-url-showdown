RABBITMQ_URL = "amqp://xavi:handball@127.0.0.1/"
EVENT_QUEUE = "events"
URL_EXCHANGE = "battle.urls"
CHROME_DRIVER_PATH = "/usr/bin/chromedriver"
PLAY_POKEMON_SHOWDOWN_URL = "https://play.pokemonshowdown.com/"
ACTIVE_BATTLES_BUTTON_XPATH = r"//*[@id='room-rooms']/div/div[1]/div/button[2]"
MINIMUM_ELO_BUTTON_XPATH = r"//*[@id='room-battles']/div/div/label/select"
MINIMUM_ELO_VALUE = "1100"
REFRESH_BATTLES_BUTTON_XPATH = r"//*[@id='room-battles']/div/div/p[1]/button"
FORMAT_SELECTION_BUTTON_XPATH = r"//*[@id='room-battles']/div/div/p[2]/button"
BATTLE_FORMATS_XPATH = {
    "OU": r"/html/body/div[5]/ul[1]/li[5]/button",
    "Ubers": r"/html/body/div[5]/ul[1]/li[7]/button",
    "UU": r"/html/body/div[5]/ul[1]/li[8]/button",
    "RU": r"/html/body/div[5]/ul[1]/li[9]/button",
    "NU": r"/html/body/div[5]/ul[1]/li[10]/button",
    "Monotype": r"/html/body/div[5]/ul[1]/li[13]/button",
    "VGC 2021": r"/html/body/div[5]/ul[1]/li[25]/button",
    "VGC 2020": r"/html/body/div[5]/ul[1]/li[26]/button",
    "Doubles OU": r"/html/body/div[5]/ul[1]/li[22]/button",
    "Doubles Ubers": r"/html/body/div[5]/ul[1]/li[23]/button",
    "Doubles UU": r"/html/body/div[5]/ul[1]/li[24]/button"
}
ROOM_BATTLES_LINK_XPATH = r"//*[@id='room-battles']/div/div/div/div/a/@href"
