import asyncio
import aio_pika


class Demo:
    RABBITMQ_HOST = "amqp://demo_user:demo_pass@127.0.0.1/"
    RABBITMQ_PORT = 5672
    EVENT_QUEUE = "events"
    URL_EXCHANGE = "events"

    def __init__(self):
        self.received_events = asyncio.Queue()
        self.connection = None
        self.pika_channel = None
        self.queue = None
        self.exchange = None

    async def on_message(self, body: aio_pika.IncomingMessage):
        """
        on_message doesn't necessarily have to be defined as async.
        Here it is to show that it's possible.
        """

        print(f" [x] Received message")
        await self.received_events.put(body)

    async def connect(self, the_loop):
        self.connection = await aio_pika.connect_robust(self.RABBITMQ_HOST,loop=the_loop)
        self.pika_channel = await self.connection.channel()
        self.queue = await self.pika_channel.get_queue(self.EVENT_QUEUE)
        self.exchange = await self.pika_channel.get_exchange(self.URL_EXCHANGE)

    async def consume(self):

            await asyncio.sleep(2)
            print("consuming")
            await self.queue.consume(self.on_message, no_ack=True)

    async def publish(self):

        while True:
            await asyncio.sleep(5)
            await self.exchange.publish(
                aio_pika.Message(b"Hi"),
                routing_key="",
            )

    async def read_val(self):
        while True:
            # await asyncio.sleep(2)
            try:
                last_message = await self.received_events.get()
                print(f"received message: {last_message.body.decode('utf-8')}")
            except IndexError:
                pass


loop = asyncio.get_event_loop()
demo = Demo()
futures = asyncio.gather(demo.connect(the_loop=loop), demo.consume(), demo.publish(), demo.read_val())
loop.run_until_complete(futures)
