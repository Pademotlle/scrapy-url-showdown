import asyncio
import constants

from scrapyscript import Job, Processor
from scrappy_worker.spiders.spinarak import Spinarak


class WebScraper:
    """
    Worker that receives an event to download all battles from a concrete battle format
    """

    def __init__(self):
        """
        Class constructor
        """

        self.battle_urls_queue = asyncio.Queue(maxsize=10000)
        self.last_battle_format = None

    @property
    async def get_battle_urls(self):

        return await self.battle_urls_queue.get()

    async def scrape(self, battle_format: str):
        """
        This method scrapes the current Pokemon battles and retrieves their URLs

        Args:
            battle_format: string that contains the battle format
        """

        if battle_format not in constants.BATTLE_FORMATS_XPATH:

            return None

        spinarak_job = Job(Spinarak, battle_format=battle_format)
        scrapy_process = Processor(settings=None)
        result = scrapy_process.run([spinarak_job])
        print(result)
        await self.battle_urls_queue.put(result)
