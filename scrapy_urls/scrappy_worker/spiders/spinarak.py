import constants
import scrapy
import time

from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from shutil import which


class Spinarak(scrapy.Spider):
    """
    Spider Class
    """

    name = "spinarak"
    start_urls = [constants.PLAY_POKEMON_SHOWDOWN_URL]

    def __init__(self, **kwargs):
        """
        Class constructor
        """

        super(Spinarak, self).__init__(**kwargs)

        self.url_battles = []
        self.battle_format = ""
        self.driver = None

        self.load_driver_settings()
        self.open_pokemon_showdown()
        self.select_battles_viewer()
        self.select_minimum_elo()
        self.select_battle_format(battle_format_name=kwargs.get("battle_format"))
        time.sleep(1)
        self.html = self.driver.page_source

        self.driver.close()

    def load_driver_settings(self):
        """
        This method just loads the selenium webdriver settings before usage
        """
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_path = which("chromedriver")
        self.driver = webdriver.Chrome(executable_path=chrome_path, options=chrome_options)

    def open_pokemon_showdown(self):
        """
        This method uses selenium to open pokemon showdown web page
        """

        print(f"Opening {constants.PLAY_POKEMON_SHOWDOWN_URL}")
        self.driver.get(constants.PLAY_POKEMON_SHOWDOWN_URL)

    def select_battles_viewer(self):
        """
        This method just selects the 'Active Battles' viewer
        """

        time.sleep(3)
        print(f"Selecting battle viewer...")
        web_element = self.driver.find_element_by_xpath(constants.ACTIVE_BATTLES_BUTTON_XPATH)
        web_element.click()

    def select_minimum_elo(self):
        """
        This method selects a minimum ELO rating
        """

        time.sleep(1)
        print(f"Setting minimum ELO to {constants.MINIMUM_ELO_VALUE}")
        selector_element = Select(self.driver.find_element_by_xpath(constants.MINIMUM_ELO_BUTTON_XPATH))
        selector_element.select_by_value(constants.MINIMUM_ELO_VALUE)

    def select_battle_format(self, battle_format_name: str):
        """
        This method selects the Battle format inside the Battles viewer

        Args:
            battle_format_name: the desired Pokemon battle format to be selected
        """

        self.battle_format = battle_format_name
        print(f"Selecting battle format...")
        web_element = self.driver.find_element_by_xpath(constants.FORMAT_SELECTION_BUTTON_XPATH)
        web_element.click()
        web_element = self.driver.find_element_by_xpath(constants.BATTLE_FORMATS_XPATH[battle_format_name])
        web_element.click()

    def refresh_battles_list(self):
        """
        This method selects the Refresh button in order to update the current Pokemon battles
        """

        web_element = self.driver.find_element_by_xpath(constants.REFRESH_BATTLES_BUTTON_XPATH)
        web_element.click()

    def parse(self, response):
        """
        This method parses the battle urls
        """

        html_response = Selector(text=self.html)

        for battle_room_element in html_response.xpath(constants.ROOM_BATTLES_LINK_XPATH):
            battle_url = constants.PLAY_POKEMON_SHOWDOWN_URL + battle_room_element.get()
            self.url_battles.append(battle_url)

        return {self.battle_format: self.url_battles}
